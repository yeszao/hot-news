
create table "news_baidu" (
  "id" integer not null primary key autoincrement,
  "title" text not null default '',
  "link" text not null default '',
  "link_crc32" integer not null default '0',
  "sort" integer not null default '0',
  "index" integer not null default '0',
  "is_new" integer not null default '0',
  "is_rise" integer not null default '0',
  "create_time" integer not null default '0',
  "update_time" integer not null default '0'
);

create index "news_baidu_link_crc32"
on "news_baidu" (
  "link_crc32" collate binary asc
);

create table "news_qq" (
  "id" integer not null primary key autoincrement,
  "title" text not null default '',
  "link" text not null default '',
  "link_crc32" integer not null default '0',
  "preview_images" text not null default '',
  "sort" integer not null default '0',
  "author" text not null default '',
  "author_url" text  not null default '',
  "publish_time" integer not null default '0',
  "create_time" integer not null default '0',
  "update_time" integer not null default '0'
);

create index "news_qq_link_crc32"
on "news_qq" (
  "link_crc32" collate binary asc
);

create table "news_weibo" (
  "id" integer not null primary key autoincrement,
  "title" text not null default '',
  "link" text not null default '',
  "link_crc32" integer not null default '0',
  "preview_images" text not null default '',
  "sort" integer not null default '0',
  "author" text not null default '',
  "author_avatar" text  not null default '',
  "author_url" text  not null default '',
  "publish_time" integer not null default '0',
  "share_count" integer not null default '0',
  "reply_count" integer not null default '0',
  "like_count" integer not null default '0',
  "create_time" integer not null default '0',
  "update_time" integer not null default '0'
);

create index "news_weibo_link_crc32"
on "news_weibo" (
  "link_crc32" collate binary asc
);