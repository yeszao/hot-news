# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BaiduItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    link = scrapy.Field()
    index = scrapy.Field()
    sort = scrapy.Field()
    is_new = scrapy.Field()
    is_rise = scrapy.Field()
    update_time = scrapy.Field()


class QQItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    link = scrapy.Field()
    preview_images = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
    sort = scrapy.Field()
    author = scrapy.Field()
    author_url = scrapy.Field()
    publish_time = scrapy.Field()
    update_time = scrapy.Field()


class WeiboItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field()
    link = scrapy.Field()
    preview_images = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
    sort = scrapy.Field()
    author = scrapy.Field()
    author_url = scrapy.Field()
    author_avatar = scrapy.Field()
    publish_time = scrapy.Field()
    share_count = scrapy.Field()
    reply_count = scrapy.Field()
    like_count = scrapy.Field()
    update_time = scrapy.Field()
