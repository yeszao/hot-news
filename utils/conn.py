import sqlite3
import os


def get_db():
    here = os.path.dirname(os.path.dirname(__file__))
    conn = sqlite3.connect(os.path.join(here, "data/data.sqlite"))
    conn.row_factory = dict_factory
    return conn


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d









