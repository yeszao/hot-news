from abc import ABCMeta, abstractmethod
import binascii


class Save(metaclass=ABCMeta):
    @abstractmethod
    def add(self, item):
        pass

    @abstractmethod
    def update(self, id, item):
        pass

    def is_link_crawled(self, link):
        params = (binascii.crc32(link.encode()), link)
        sql = "select id from $table where link_crc32=? and link=? order by id limit 1"

        result = self.query_one(sql, params)

        return result["id"] if result else False
