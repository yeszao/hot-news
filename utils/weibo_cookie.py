import time
import requests
import logging
import random
from scrapy.exceptions import CloseSpider

"""
https://blog.thinker.ink/passage/28/
https://blog.csdn.net/qq_41057280/article/details/81201337
"""

user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 " \
             "(KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"


def get_cookies():
    cookies = __get_final_cookie()

    i = 1
    while cookies is None:
        logging.info("Try to get cookie %s time failed, retrying..." % str(i))
        time.sleep(1)
        cookies = __get_final_cookie()
        i += 1
        if i == 7:
            logging.error("Cannot get visitor cookie from weibo, close spider!!!")
            raise CloseSpider()

    return cookies


def __get_tid():
    """
    获取tid,c,w
    :return:tid
    """
    tid_url = "https://passport.weibo.com/visitor/genvisitor"
    data = {
        "cb": "gen_callback",
        "fp": {
            "os": "3",
            "browser": "Chrome69,0,3497,100",
            "fonts": "undefined",
            "screenInfo": "1920*1080*24",
            "plugins": "Portable Document Format::internal-pdf-viewer::Chrome PDF Plugin|::mhjfbmdgcfjbbpaeojofohoefgiehjai::Chrome PDF Viewer|::internal-nacl-plugin::Native Client"
        }
    }
    req = requests.post(url=tid_url, data=data, headers={'User-Agent': user_agent})

    if req.status_code == 200:
        ret = eval(
            req.text.replace("window.gen_callback && gen_callback(", "").replace(");", "").replace("true", "1"))
        return ret.get('data').get('tid')
    return None


def __get_final_cookie():
    """
    获取完整的cookie
    :return: cookie
    """
    tid = __get_tid()
    if not tid:
        return None

    cookies = {
        "tid": tid + "__095"  # + tid_c_w[1]
    }
    url = "https://passport.weibo.com/visitor/visitor?a=incarnate&t={tid}" \
          "&w=2&c=095&gc=&cb=cross_domain&from=weibo&_rand={rand}"
    req = requests.get(url.format(tid=tid, rand=random.random()),
                       cookies=cookies, headers={'User-Agent': user_agent})
    if req.status_code != 200:
        return None

    ret = eval(req.text.replace("window.cross_domain && cross_domain(", "").replace(");", "").replace("null", "1"))

    try:
        sub = ret['data']['sub']
        if sub == 1:
            return None
        subp = ret['data']['subp']
    except KeyError:
        return None

    return req.cookies.get_dict()




