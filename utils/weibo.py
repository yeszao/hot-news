import binascii
import json
from utils import extract_images, get_weibo_time, extract_avatar, extract_time
from utils.database import Model
from utils.save import Save


class Weibo(Model, Save):
    table = "news_weibo"

    def add(self, item):
        preview_images = item["images"][0:-1]
        avatar = item["images"][-1]

        self.do_insert({
            "title": item["title"].strip(),
            "link": item["link"].strip(),
            "link_crc32": binascii.crc32(item["link"].encode()),
            "preview_images": json.dumps(preview_images),
            "sort": int(item["sort"]),
            "author": item["author"],
            "author_url": item["author_url"],
            "author_avatar": json.dumps(avatar),
            "publish_time": get_weibo_time(item["publish_time"]),
            "share_count": int(item["share_count"]),
            "reply_count": int(item["reply_count"]),
            "like_count": int(item["like_count"]),
            "create_time": item["update_time"],
            "update_time": item["update_time"]
        })

    def update(self, id, item):
        preview_images = item["images"][0:-1]

        params = {
            "update_time": item["update_time"],
            "preview_images": json.dumps(preview_images),
            "sort": int(item["sort"]),
        }

        self.do_update(params, {"id": id})

    def get_tops(self, num):
        sql = "select * from $table order by update_time desc, `sort` asc limit ?"
        params = (num,)

        result = self.query_all(sql, params)

        extract_images(result)

        extract_avatar(result)

        extract_time(result)

        return result
