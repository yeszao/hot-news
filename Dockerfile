FROM python:3.7

COPY requirement.txt /tmp/requirement.txt

COPY pip.conf /etc/pip.conf

RUN pip install -r /tmp/requirement.txt

WORKDIR /works