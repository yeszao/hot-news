# coding: utf-8

import sys
from scrapy import cmdline

spider = sys.argv[1]

cmdline.execute(("scrapy crawl %s" % spider).split())
