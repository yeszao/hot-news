# 说明

## 技术栈
* 编程语言：python
* 采集组件：scrapy + splash
* Web组件：flask
* 数据存储：sqlite，保存在data目录中。


## 使用说明

1. 基于模板创建数据库文件。
    ```bash
    cp data/data-example.sqlite data.sqlite
    ```
2. 安装python依赖。
    ```bash
    pip install -r requirment.txt
    ```
3. 启动Flask：
    ```bash
    python run.py
    ```
    通过浏览器访问 `localhost:9000` 即可访问UI界面。
    Flask使用`9000`端口启动，也可修改run.py来指定另外的端口。
    
## 采集说明
因为腾讯新闻和微博使用ajax加载技术，所以需要通过splash模拟浏览器行为。

采集腾讯新闻和微博之前，本地测试时，需要先启动splash（需要先安装docker）：

```bash
docker-compose up splash
```
再在web界面点击采集, splash默认使用`8050`端口。

## 部署
线上部署直接使用docker容器，具体配置请看 docker-compose.yml 文件。

```bash
docker-compose up
```

然后通过`地址加端口`即可访问。
